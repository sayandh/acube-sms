package com.kotlincodes.acubesms.activity


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import com.kotlincodes.acubesms.MainActivity
import com.kotlincodes.acubesms.R

import kotlinx.android.synthetic.main.activity_otp_verify.*

class OTPActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verify)
        // Set up the login form.

        btn_otp_verify.setOnClickListener {
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
    }
}
