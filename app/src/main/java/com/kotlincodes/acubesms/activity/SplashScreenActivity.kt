package com.kotlincodes.acubesms.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.kotlincodes.acubesms.R

class SplashScreenActivity : AppCompatActivity() {
    private val SPLASH_TIME_OUT = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        Handler().postDelayed({

            startActivity(Intent(this,LoginActivity::class.java))
            finish()

        }, SPLASH_TIME_OUT.toLong())

    }

    private fun checkLogin() {

        when {
//            sharedPreference!!.getValueBoolien(Pref.PREF_1ST_OPEN, true) -> {
//                startActivity(Intent(this, WelcomeActivity::class.java))
//                overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
//                finish()
//            }
//            sharedPreference!!.getValueBoolien(Pref.PREF_LOGIN_STATUS, false) -> {
//                startActivity(Intent(this, HomeActivity::class.java))
//                overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
//                finish()
//            }
//            else -> {
//                startActivity(Intent(this, LoginActivity::class.java))
//                overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
//                finish()
//            }
        }
    }
}