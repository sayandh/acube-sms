package com.kotlincodes.acubesms

import android.content.pm.PackageManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.kotlincodes.acubesms.model.SmsModel

import com.kotlincodes.acubesms.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val smsModel=readSMS()

        txt_total_count.text="Total SMS count : ${smsModel.totalCount}"
        txt_total_unread.text="Un Read SMS count : ${smsModel.unreadSmsCount}"

        recyclerViewHome.adapter=SmsAdapter(this,smsModel.details)
        recyclerViewHome.layoutManager=LinearLayoutManager(this)



    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        try {
            if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val smsModel=readSMS()
                recyclerViewHome.adapter=SmsAdapter(this,smsModel.details)
            } else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        } catch (e: ArrayIndexOutOfBoundsException) {

        }

    }

    fun readSMS():SmsModel{
        val smsModel=SmsModel()
        if (!Utils.checkPermissionForReadSms(this)) {
            Utils.requestPermissionForReadSMS(this)
        }else{
            val listData=ArrayList<SmsModel.Details>()
            val cursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null)

            smsModel.totalCount=cursor.count
            var unReadCount=0
            if (cursor!!.moveToFirst()) {
                // must check the result to prevent exception
                do {
                    var msgData = ""
                    val smsModelDetails=SmsModel.Details()
                    for (idx in 0 until cursor.columnCount) {
                        msgData += " " + cursor.getColumnName(idx) + ":" + cursor.getString(idx)

                        when {
                            cursor.getColumnName(idx).equals("address",true) -> smsModelDetails.address= cursor.getString(idx)
                            cursor.getColumnName(idx).equals("date",true) -> smsModelDetails.date= cursor.getString(idx)
                            cursor.getColumnName(idx).equals("date_sent",true) -> smsModelDetails.dateSent= cursor.getString(idx)
                            cursor.getColumnName(idx).equals("read",true) -> smsModelDetails.readStatus= cursor.getInt(idx)
                            cursor.getColumnName(idx).equals("body",true) -> smsModelDetails.body= cursor.getString(idx)
                        }

                    }
                    if(smsModelDetails.body.contains("credited")||
                            smsModelDetails.body.contains("debited") ||
                            smsModelDetails.body.contains("withdrawn")) {
                        listData.add(smsModelDetails)
                    }
                    if (smsModelDetails.readStatus==0){
                        unReadCount++
                    }

                } while (cursor.moveToNext())

                cursor.close()
                smsModel.details=listData
                smsModel.unreadSmsCount=unReadCount
            } else {
                // empty box, no SMS
            }

        }

        if (smsModel.details.isEmpty()){
            txt_heading.text=getString(R.string.no_trans)
        }else{
            txt_heading.text="Financial Transactions : ${smsModel.details.size} "
        }
        return smsModel
    }
}
