package com.kotlincodes.acubesms.model

import java.net.Inet4Address

class SmsModel {
    var details:List<Details> = listOf()
    var totalCount=0
    var unreadSmsCount=0

     class Details {
        var address=""
        var date=""
        var dateSent=""
        var readStatus:Int=0
        var body=""
    }
}