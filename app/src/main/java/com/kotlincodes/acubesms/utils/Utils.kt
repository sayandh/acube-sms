package com.kotlincodes.acubesms.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat


class Utils{
    companion object{
        fun checkPermissionForReadSms(context: Context): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                val result = context.checkSelfPermission(Manifest.permission.READ_SMS)
                result == PackageManager.PERMISSION_GRANTED
            } else {
                true
            }
        }

        fun requestPermissionForReadSMS(context: Context) {
            try {
                ActivityCompat.requestPermissions(
                        context as Activity, arrayOf(Manifest.permission.READ_SMS),
                        1
                )
            } catch (e: Exception) {
                e.printStackTrace()
                throw e
            }

        }
    }


//    companion object{
//        fun checkPermissionForReadSms(context: Context): Boolean {
//            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
//                val result = context.checkSelfPermission(Manifest.permission.READ_SMS)
//                result == PackageManager.PERMISSION_GRANTED
//            } else {
//                true
//            }
//        }
//
//        fun requestPermissionForReadSMS(context: Context) {
//            try {
//                ActivityCompat.requestPermissions(
//                        context as Activity, arrayOf(Manifest.permission.READ_SMS),
//                        1
//                )
//            } catch (e: Exception) {
//                e.printStackTrace()
//                throw e
//            }
//
//        }
//    }

}