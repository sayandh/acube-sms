package com.kotlincodes.acubesms

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kotlincodes.acubesms.model.SmsModel

class SmsAdapter(val context: Context, val smsList:List<SmsModel.Details>) : RecyclerView.Adapter<SmsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view =LayoutInflater.from(context).inflate(R.layout.list_item_sms,p0,false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return smsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        val rowItem=smsList[p1]
        holder.txtSender.text=rowItem.address
        holder.txtBody.text=rowItem.body

    }

    inner class ViewHolder(rootView: View) : RecyclerView.ViewHolder(rootView) {
        val txtSender=rootView.findViewById<TextView>(R.id.txt_sender)
        val txtBody=rootView.findViewById<TextView>(R.id.txt_body)
    }
}